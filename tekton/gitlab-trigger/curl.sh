curl -v -k \
-H 'X-GitLab-Token: token' \
-H 'X-Gitlab-Event: Push Hook' \
-H 'Content-Type: application/json' \
--data-binary "@example-payload.json" \
example.com