#!/usr/bin/env bash
set -x

kubectl delete namespace hello-world-pipeline
kubectl create namespace hello-world-pipeline

kubectl -n hello-world-pipeline apply -f ./secret.yaml
kubectl -n hello-world-pipeline apply -f ./task-deploy-local.yaml
kubectl -n hello-world-pipeline apply -f ./task-deploy-remote.yaml
kubectl -n hello-world-pipeline apply -f ./task-build-and-push.yaml
kubectl -n hello-world-pipeline apply -f ./task-unit-test.yaml
kubectl -n hello-world-pipeline apply -f ./pipeline-role.yaml
kubectl -n hello-world-pipeline apply -f ./pipeline.yaml

kubectl -n hello-world-pipeline apply -f ./gitlab-trigger/admin-role.yaml
kubectl -n hello-world-pipeline apply -f ./gitlab-trigger/webhook-role.yaml
kubectl -n hello-world-pipeline apply -f ./gitlab-trigger/create-ingress.yaml
kubectl -n hello-world-pipeline apply -f ./gitlab-trigger/gitlab-push-listener.yaml
# kubectl -n hello-world-pipeline create -f ./gitlab-trigger/ingress-run.yaml

