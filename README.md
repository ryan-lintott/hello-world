# Tekton Example Pipeline

## Dependencies

All the following should be installed on k8s cluster

- Tekton Pipelines

```
kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
```

- Tekton Triggers

```
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml
```

- nginx ingress controller

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/baremetal/deploy.yaml
```

- Tekton Dashboard

```
kubectl apply --filename https://storage.googleapis.com/tekton-releases/dashboard/latest/tekton-dashboard-release.yaml
```

## Guide

To install the pipeline onto the cluster run 
`./tekton/install-pipeline.sh`

Right now there is a deployment pipeline that can be run manually by running
`kubectl -n hello-world-pipeline create -f tekton/pipeline-run.yaml`

```
test 

 | 
 | (on success)
 V

build and push to docker

 |
 | (on success)
 V

deploy chart in this repository's helm directory to chosen env (dev, stg, prd)

```


There is also a tekton trigger set up that listens to push events to this repository and runs the `unit-test` task for the commit that was pushed, to any branch


## Tekton Dashboard

Run `kubectl proxy`

Then visit `http://localhost:8001/api/v1/namespaces/tekton-pipelines/services/tekton-dashboard:http/proxy/#/about`

